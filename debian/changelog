cmus (2.11.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.11.0
    - Fix build with ffmpeg 7.0 (Closes: #1072405)
  * debian/patches: Refresh patches
  * debian/control:
    - Bump Standards-Version
    - Use pkgconf
    - Use openmpt's modplug
    - Use libncurses-dev

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 09 Jun 2024 23:45:17 +0200

cmus (2.10.0-4) unstable; urgency=medium

  * Team upload
  * debian/patches: Apply upstream patch to fix compatibility with ffmpeg 6.0
    (Closes: #1041375)

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 27 Jul 2023 21:00:31 +0200

cmus (2.10.0-3) unstable; urgency=medium

  * Team upload
  * debian/control: Bump Standards-Version
  * debian/: Remove roar support (Closes: #1030661)

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 07 Feb 2023 09:20:50 +0100

cmus (2.10.0-2) unstable; urgency=medium

  * Team upload
  * Add a patch from upstream to fix a freeze when exiting cmus

 -- Philippe SWARTVAGHER <phil.swart@gmx.fr>  Sun, 31 Jul 2022 22:01:04 +0200

cmus (2.10.0-1) unstable; urgency=medium

  * Team upload

  [ Jenkins ]
  * Remove constraints unnecessary since buster

  [ Philippe SWARTVAGHER ]
  * New upstream version 2.10.0
  * Bump d/watch version
  * Bump standards-version to 4.6.1
  * Add a patch to fix a typo spotted by Lintian

 -- Philippe SWARTVAGHER <phil.swart@gmx.fr>  Wed, 13 Jul 2022 20:40:37 +0200

cmus (2.9.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 22 Jan 2021 22:28:04 +0100

cmus (2.9.0-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Patrick Gaskin ]
  * Fix missing dependency for MPRIS support

  [ Sebastian Ramacher ]
  * New upstream release
  * debian/control:
    - Bump Standards-Version
    - Bump debhelper compat to 13
    - Set RRR: no
  * debian/patches/12-typos.patch: Removed, fixed upstream

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 19 Jan 2021 20:15:26 +0100

cmus (2.8.0-2) unstable; urgency=medium

  * Set build flags via /usr/share/dpkg/buildflags.mk
  * Link with -latomic to fix FTBFS on various architectures (Closes: #935678)

 -- Ryan Kavanagh <rak@debian.org>  Sat, 07 Sep 2019 10:37:13 -0400

cmus (2.8.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Helmut Grohne ]
  * Fix FTCBFS: Supply a cross environment to ./configure. (Closes: #911163)

  [ Ryan Kavanagh ]
  * New upstream version 2.8.0 (Closes: #932107, #783498)
    + Update copyright file with new holders
    + Drop 12-ffmpeg-4.0.patch (applied upstream)
    + Refresh patches
  * Drop outdated get-orig-source target from rules
  * Fix typos in source, 12-typos.patch
  * Enable hardening
  * Bump debphelper compat to 12
  * Bump standards-version to 4.4.0
  * cmus-plugin-ffmpeg (<< 2.8.0) breaks cmus (>= 2.8.0)

 -- Ryan Kavanagh <rak@debian.org>  Sat, 17 Aug 2019 14:51:27 -0400

cmus (2.7.1+git20160225-2) unstable; urgency=medium

  * Team upload.

  [ James Cowgill ]
  * Add upstream patch to fix FTBFS with FFmpeg 4.0. (Closes: #888384)
  * d/changelog: Remove trailing blank line.

  [ Alessio Treglia ]
  * Remove myself from the Uploaders field.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/changelog: Remove trailing whitespaces.

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org.

 -- James Cowgill <jcowgill@debian.org>  Thu, 17 May 2018 11:34:35 +0100

cmus (2.7.1+git20160225-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot.
    - Fix build against ffmpeg 3.0. (Closes: #810557)
  * debian/patches/{01_config.mk.diff,02_link_avcodec.patch}: Removed, applied
    upstream.
  * debian/control:
    - Bump Standards Version.
    - Update Vcs-Git.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 06 Mar 2016 21:37:57 +0100

cmus (2.7.1-1) unstable; urgency=medium

  * Team upload.

  [ Alessio Treglia ]
  * Demote extra plugins to Suggests (Closes: #789256)
  * Refresh patchset for 2.6.0.

  [ Sebastian Ramacher ]
  * New upstream release. (Closes: #779335, #792134)
    - Use libswresample instead of libavresample. (Closes: #805169, #805109)
  * Update path for README
  * debian/control:
    - Add libdiscid-dev, libopusfile-dev, libsamplerate0-dev and libjack-dev
      to Build-Depends.
    - Change libavresample-dev to libswresample-dev in Build-Depends and add
      libavcodec-dev.
    - Add bash-completion to Build-Depends.
    - Bump Standards-Version to 3.9.6.
    - Update Vcs-Browser.
    - Make cmus-plugin-ffmpeg depend on the same version of cmus (Closes:
      #695072)
  * debian/cmus.install: Install zsh completion.
  * debian/cmus.bash-completion: Install bash completion.
  * debian/rules:
    - Build with --parallel and --with bash-completion.
    - Handle jack shlibs similar to pulse.
  * debian/patches:
    - libav10.patch: Removed, no longer needed.
    - 02_link_avcodec.patch: Functions from libavcodec are used so make sure
      the ffmpeg plugin is linked against libavcodec.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 15 Nov 2015 19:24:52 +0100

cmus (2.5.0-7) unstable; urgency=medium

  * Re-introduce Roaraudio support (Closes: #680745):
    - debian/control: Add build-dependency on libroad-dev.
    - debian/rules: Tune dpkg-shlibdeps call to move pulse and roar's
      dependencies to Recommends. Made the whole mechanism slightly more
      elegant.
  * Enable CUE support.
  * The project has moved to github, update the Homepage field accordingly.
  * Update debian/watch, project has moved from sourceforge to github.

 -- Alessio Treglia <alessio@debian.org>  Thu, 14 Aug 2014 13:45:10 +0100

cmus (2.5.0-6) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 11 May 2014 23:45:16 +0200

cmus (2.5.0-5) experimental; urgency=low

  * Team upload.
  * Compile against libav10 (Closes: #739301)
  * Bump standards version

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 24 Mar 2014 19:35:28 -0400

cmus (2.5.0-4) unstable; urgency=low

  [ Ryan Kavanagh ]
  * Patches were applied upstream

  [ Alessio Treglia ]
  * Add patch to prevent FTBFS. (Closes: #724181)

 -- Alessio Treglia <alessio@debian.org>  Sun, 06 Oct 2013 20:46:25 +0100

cmus (2.5.0-3) unstable; urgency=low

  * Don't FTBFS due to missing config.mk (Closes: #720781), 01_config.mk.diff
  * Fix typo in cmus binary, 02_fix_typo.diff
  * Enable build hardening
    + Bump debhelper version to 9.0.0 and compat to 9
    + Introduce 03_cppflags.diff to use CPPFLAGS; needed for function
      fortification
    + Override hardening-no-fortify-functions false positives
  * Bump standards version to 3.9.4
  * Switch to canonical Vcs-* fields
  * Enable verbose build logs

 -- Ryan Kavanagh <rak@debian.org>  Thu, 29 Aug 2013 13:48:30 -0400

cmus (2.5.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Alessio Treglia <alessio@debian.org>  Sat, 11 May 2013 01:21:18 +0200

cmus (2.5.0-1) experimental; urgency=low

  * New upstream release:
    - CUE sheets support.
    - cdio input plugin.
    - support for WavPack `.wvc` correction files.
    - new «zenburn» color scheme and text attributes (bold/reverse/...)
      support for UI elements.
    - improved tab completion, new scroll_offset and icecast_default_charset
      options, even better tag parsing and compilations handling, and
      numerous small enhancements all over the place.
  * Build-depend on libcddb2-dev,libcdio-cdda-dev.

 -- Alessio Treglia <alessio@debian.org>  Thu, 15 Nov 2012 00:28:20 +0000

cmus (2.4.3-2) unstable; urgency=low

  [ Ryan Kavanagh ]
  * Update my email address to @debian.org
  * Drop DM-Upload-Allowed: yes, no longer needed

  [ Alessio Treglia ]
  * Build cmus without roar support. (Closes: #675610)
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sat, 02 Jun 2012 20:07:57 +0200

cmus (2.4.3-1) unstable; urgency=low

  * New upstream release.

 -- Alessio Treglia <alessio@debian.org>  Sat, 03 Dec 2011 12:55:46 +0100

cmus (2.4.2-1) unstable; urgency=low

  * New upstream release.
  * Drop 0001-fix-compile-error-for-new-versions-of-ffmpeg.patch,
    applied upstream.

 -- Alessio Treglia <alessio@debian.org>  Tue, 26 Jul 2011 10:13:25 +0200

cmus (2.4.1-2) unstable; urgency=low

  * Add Ubuntu-specific patch to fix FTBFS with newest version of ffmpeg.
  * Replace negated list of architectures with linux-any (Closes: #634706).

 -- Alessio Treglia <alessio@debian.org>  Sat, 23 Jul 2011 10:48:29 +0200

cmus (2.4.1-1) unstable; urgency=medium

  * New upstream release (Closes: #628422):
    - Doc: add help for :shell
    - ffmpeg: move up "config/ffmpeg.h" include
    - fix two memleaks
    - fix cache refresh bug
    - configure: fix FLAC include path
    - configure: fix ffmpeg header detection
    - fix TCP/IP networking protocol
    - fix segfault when hitting win-activate on empty tree
    - display error if seeking failed
    - fix segfault when using tqueue/lqueue
    - fix lqueue command
    - fix infinite loop when adding certain mp3 files
    - fix reading of id3v2 tags at the end of files
    - more fault-tolerant integer tag-reading
  * Bump urgency to medium as the previous release was seriously buggy.

 -- Alessio Treglia <alessio@debian.org>  Sun, 29 May 2011 19:18:48 +0200

cmus (2.4.0-1) unstable; urgency=low

  * New upstream release "Easter egg":
    - Mutt-like short filters.
    - Live filtering.
    - Resume support.
    - Smarter string handling.
    - Long format options, including ones for bitrate/codec.
    - HTTP proxy support for streams via http_proxy environment variable.
    - Less CPU wakeups during playback.
    - New RoarAudio output plugin.
    - Support for big-endian systems, lots of different audio sample formats,
      almost any C compiler and unix-like OS out there.
    - Various bugfixes.
    - Full release notes are available at:
      http://sourceforge.net/mailarchive/message.php?msg_id=27403242
  * debian/watch: Properly handle release-candidate,beta releases.
  * Remove debian/patches directory, all patches have been applied upstream.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 26 Apr 2011 23:30:07 +0200

cmus (2.3.5-1) unstable; urgency=low

  * New upstream release:
    - Features gapless MP3 playback.
    - Native PulseAudio support.
    - Faster startup.
    - Improve buildsystem.
  * Refresh patches.
  * Remo 21-missing_plugins.patch, applied upstream.

 -- Alessio Treglia <alessio@debian.org>  Sat, 23 Apr 2011 09:56:57 +0200

cmus (2.3.4-3) unstable; urgency=low

  * Handle missing dependencies more gracefully:
    - cmus silently skips plugins with missing dependencies, and instead
      outputs a debug message. Original patch by Johannes Weißl, already
      accepted upstream.

 -- Alessio Treglia <alessio@debian.org>  Fri, 01 Apr 2011 08:59:01 +0200

cmus (2.3.4-2) unstable; urgency=low

  * Avoid to depend on several sound servers (Closes: #612887) and let
    users choose to rely on the favorite one.
    - debian/control:
      + Add shlibs:Recommends field.
    - debian/rules:
      + Supply {dh_,dpkg-}shlibdeps with proper options to demote roar
        and pulse audio dependencies to Recommends.

 -- Alessio Treglia <alessio@debian.org>  Tue, 15 Mar 2011 12:46:03 +0100

cmus (2.3.4-1) unstable; urgency=low

  [ Ryan Kavanagh ]
  * New upstream release.
  * Dropped 01_spelling_mistakes.diff, 02_cmus-tutorial_whatis.diff
    and 03-terminal_corruption.patch (applied upstream).
  * Refreshed 10-roaraudio_support.patch
  * Bump my copyright

  [ Alessio Treglia ]
  * Add DM-Upload-Allowed: yes.

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Tue, 22 Feb 2011 09:03:23 -0500

cmus (2.3.3-4) unstable; urgency=low

  * Upload to unstable.

 -- Alessio Treglia <alessio@debian.org>  Wed, 09 Feb 2011 12:05:49 +0100

cmus (2.3.3-3) experimental; urgency=low

  * Add RoarOutput plugin (Closes: #609202), thanks to
    Philipp Schafft <lion@lion.leolix.org> for the patch.
  * Add patch taken from upstream's git to fix segfault when adding to
    queue.
  * Build-depends on libroar-dev (>= 0.4~beta2).

 -- Alessio Treglia <alessio@debian.org>  Mon, 17 Jan 2011 02:23:04 +0100

cmus (2.3.3-2) unstable; urgency=low

  * Prevent terminal corruption on track change.
  * debian/copyright: Update sources download location.
  * Split cmus to provide smart dependencies (Closes: #442423).
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sun, 01 Aug 2010 12:26:08 +0200

cmus (2.3.3-1) unstable; urgency=low

  [ Ryan Kavanagh ]
  * New upstream release (Closes: #572284)
  * Imported Upstream version 2.3.3
  * Changed to source format 3.0 source. Involved converting
    dpatch->quilt and dropping dpatch B-D.
  * Dropped 01_cmusffmpeg.diff, no longer needed. Upstream checks for
    ffmpeg and falls back to libavcodec for us.
  * Dropped 02_cmusstatusdisplay.diff, included upstream.
  * Dropped Yavor's mpcdec patch. Upstream expanded on it, using his
    code if building under MPC SV8, the old code otherwise.
  * Updated debian/watch
  * Updated copyright file with new copyright holders and download
    location.
  * This is a new upload, package will thus be rebuilt against libavformat.
    (Closes: #568361).
  * Dropped debian/patches directory
  * Move to debhelper 7 rules
  * Now standards-version 3.8.4
  * Fix debhelper-but-no-misc-depends lintian warning
  * Override dh_auto_configure because upstream uses a homebrewed configure
    script that doesn't accept --a=b style options
  * Bump debhelper version to (>= 7.0.50~) because we're using override_dh_*
  * Fix spelling mistakes in binary and documentation
    (01_spelling_mistake.diff)
  * Fix whatis entry for cmus-tutorial manpage (02_cmus-tutorial_whatis.diff)
  * Added Julien Louis' and my own packaging copyright blurb to
    debian/copyright.

  [ Alessio Treglia ]
  * This isn't a non-maintainer upload, we adopt this (Closes: #587604).
  * Add debian/gbp.conf file.
  * debian/control:
    - Add Vcs-* tags.
    - Bump Standards.
    - Lines should be shorter than 80 characters.
    - Add myself to Uploaders field, I'll take care of sponsoring this in
      future.
    - Build-depend on pkg-config.
    - Drop unnecessary Recommends field.
  * Drop aRTs support as it is no longer maintained.
  * Add PulseAudio support.
  * debian/rules:
    - Call configure script instead of relying on dh_auto_configure.
  * Drop README.source, we don't rely on dpatch as patch system.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Sun, 04 Jul 2010 20:06:58 +0200

cmus (2.2.0-4.1) unstable; urgency=low

  * NMU
  * Patch from Yavor Doganov to port cmus to the new mpcdec API,
    thereby allowing cmus to build from source again.
    closes: #476382, #552820.

 -- Clint Adams <schizo@debian.org>  Sun, 31 Jan 2010 00:03:40 -0500

cmus (2.2.0-4) unstable; urgency=low

  * Updated debian/watch file Closes: #449897
    - Thanks to Raphael Geissert <atomo64@gmail.com>
  * Fix the ffmpeg/avcodec.h includes Closes: #517570
    - Thanks to Cyril Brulebois <kibi@debian.org>
  * Added Recommends libasound2, libartsc0, libao2 Closes: #439719
  * Added Depends line in debian/control
  * Added dpatch on Build-Depends on debian/control
  * Deleted commented lines on debian/rules
  * Update debian/rules for dpatch dependence
  * Changed debian/control
    - Standard-Version to 3.8.1 ( was 3.8.0 )
    - Added debian/README.source
    - Updated to debhelper to 7
    - Updated debian/compat (was 5)
    - Added Homepage field

 -- Carlos Eduardo Sotelo Pinto (krlos) <krlos.aqp@gmail.com>  Thu, 19 Mar 2009 13:38:16 -0500

cmus (2.2.0-3) unstable; urgency=low

  * Acknowledging NMU.  Closes: #509277.

 -- Carlos Eduardo Sotelo Pinto (krlos) <krlos.aqp@gmail.com>  Mon, 29 Dec 2008 22:01:01 +0100

cmus (2.2.0-2) unstable; urgency=low

  * New maintainer. Closes: #484734
  * Changed debian/control
    - Standard-Version to 3.8.0 ( was 3.7.2 no changes needed )

 -- Carlos Eduardo Sotelo Pinto (krlos) <krlos.aqp@gmail.com>  Wed, 03 Sep 2008 17:46:50 -0500

cmus (2.2.0-1.1) unstable; urgency=high

  * Non-maintainer upload by the Security Team.
  * Modify example script cmus-status-display to write the current
    status to .cmus-status in the user's home instead of /tmp/cmus-status,
    since the latter could lead to symlink attacks. CVE-2008-5375
    (Closes: #509277)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 28 Dec 2008 14:57:06 +0100

cmus (2.2.0-1) unstable; urgency=low

  * New upstream release
  * Add libwavpack-dev and libavformat.dev to Build-Depends
  * Update debian/copyright

 -- Julien Louis <ptitlouis@sysif.net>  Fri, 27 Jul 2007 21:54:36 +0200

cmus (2.1.0-2) unstable; urgency=low

  * Rebuild against libflac8 (Closes: #426638).

 -- Julien Louis <ptitlouis@sysif.net>  Mon, 04 Jun 2007 22:56:58 +0200

cmus (2.1.0-1) unstable; urgency=low

  * New upstream release (closes: #399965).
  * Updated debian/copyright
  * Added libfaad-dev to Build-Depends

 -- Julien Louis <ptitlouis@sysif.net>  Thu, 21 Dec 2006 20:25:59 +0100

cmus (2.0.4-1) unstable; urgency=low

  * New upstream release.
  * Added debian/watch file.
  * Build-Depends agains libasound2-dev (>= 1.0.11).

 -- Julien Louis <ptitlouis@sysif.net>  Wed, 23 Aug 2006 03:34:04 +0200

cmus (2.0.3-3) unstable; urgency=low

  * Drop libasound2-dev Build Dependency on non-linux arches
    (Closes: #377885).

 -- Julien Louis <ptitlouis@sysif.net>  Wed, 12 Jul 2006 01:06:02 +0200

cmus (2.0.3-2) unstable; urgency=low

  * Move all dh_* helper stuff in the binary-arch target (Closes: #376320).
  * Remove debian/patches/01_asciidoc_xsl_path.dpatch since it is not usefull.
  * Remove dpatch from Build-Depends.

 -- Julien Louis <ptitlouis@sysif.net>  Sun,  2 Jul 2006 14:16:03 +0200

cmus (2.0.3-1) unstable; urgency=low

  * New upstream release.
  * Remove asciidoc, docbook-xsl, doxbook-xml ans xsltproc from Build-Depends.
  * Add libartsc0-dev and libao-dev to Build-Depends.

 -- Julien Louis <ptitlouis@sysif.net>  Sun, 18 Jun 2006 17:26:44 +0200

cmus (2.0.2-1) unstable; urgency=low

  * New upstream release.
  * Remove ASCIIDOC patch since upstream now search for
    /etc/asciidoc/docbook-xsl/.
  * Add docbook-xml to Build-Depends.
  * Bump Standards-Version no change needed.
  * Added REAMDE.Debian

 -- Julien Louis <ptitlouis@sysif.net>  Tue, 30 May 2006 22:12:01 +0200

cmus (2.0.0-1) unstable; urgency=low

  * Initial release (Closes: #340000)

 -- Julien Louis <ptitlouis@sysif.net>  Mon,  7 Nov 2005 18:19:55 +0100
